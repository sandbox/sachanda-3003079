<?php

namespace Drupal\media_thumbnail\Form;

use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class PdfthumbSettings.
 */
class MediaThumbnailSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_thumbnail_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('media_thumbnail.settings');
    $values = $form_state->getUserInput();
    // Avoid saving form metadata values in config.
    unset($values['form_build_id']);
    unset($values['form_id']);
    unset($values['form_token']);
    unset($values['op']);
    foreach ($values as $variable => $value) {
      $config->set($variable, $value);
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['media_thumbnail.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->configFactory->get('media_thumbnail.settings');
    $selected_vocab = empty($config->get('mediathumbnail_vocab')) ? "" : $config->get('mediathumbnail_vocab');
    $form = [];
    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => t("settings"),
      '#description' => t('Warning: Currently this module only works for single valued fields.'),
    ];

    $form['settings']['mediathumbnail_convertpath'] = [
      '#type' => 'textfield',
      '#title' => t('Give the real path of convert tools'),
      '#description' => t('(Ex: /usr/bin/convert)'),
      '#default_value' => $config->get('mediathumbnail_convertpath'),
    ];

    $form['settings']['mediathumbnail_link_files'] = [
      '#type' => 'checkbox',
      '#title' => t('Link PDF with Image File. When you will delete the pdf file, the associated image file will be deleted.'),
      '#default_value' => $config->get('mediathumbnail_link_files'),
    ];
    $vocabularies = Vocabulary::loadMultiple();
    foreach ($vocabularies as $key => $value) {
      $vocabs[$key] = $value->label();

    }

    $form['settings']['mediathumbnail_vocab'] = [
      '#title' => 'Thumbnail Vocabulary',
      '#type' => 'select',
      '#description' => 'Please select vocabulary from which yoy want to select term to categorize the pdf thumbnails.',
      '#options' => $vocabs,
      '#default_value' => $selected_vocab,
      '#attributes' => [
        'class' => [
          'thumbnail-category',
        ],
      ],
      '#ajax' => [
        'callback' => '::ajaxGetTerms',
        'wrapper' => 'ajax-thumb-cat-wrapper',
        'effect' => 'fade',
        'event' => 'change',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
    ];
    $form['settings']['ajax-thumb-cat-wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'ajax-thumb-cat-wrapper'],
    ];
    if ($form_state->getValue('mediathumbnail_vocab') != FALSE) {
      $selected_vocab = $form_state->getValue('mediathumbnail_vocab');
    }
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($selected_vocab);
    $catTerms = [];
    foreach ($terms as $tree_object) {
      $catTerms[$tree_object->tid] = $tree_object->name;
    }
    $form['settings']['ajax-thumb-cat-wrapper']['mediathumbnail_category'] = [
      '#title' => 'Thumbnail Type',
      '#type' => 'select',
      '#default_value' => $config->get("mediathumbnail_category"),
      '#description' => 'Please select term to categorize the auto genereated thumbnails.',
      '#options' => $catTerms,
      '#attributes' => [
        'class' => [
          'pdf-category',
        ],
      ],
    ];
    $form['entity_config'] = [
      '#type' => 'fieldset',
      '#title' => t("Fields configuration"),
      '#description' => t('For each entity, match a PDF file field with an image field or the Media Library'),
    ];
    // For each entity.
    $entityDefinitions = \Drupal::entityTypeManager()->getDefinitions();

    foreach ($entityDefinitions as $definition) {
      $class = $definition->getOriginalClass();
      $fieldable = is_a($class, 'Drupal\Core\Entity\FieldableEntityInterface', TRUE);
      $is_entity_type = $definition->getBundleEntityType();
      // Show only fieldable entities.
      if ($fieldable && $is_entity_type) {
        $form['entity_config'][$definition->id()] = [
          '#type' => 'fieldset',
          '#title' => $definition->getLabel(),
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
        ];

        $bundles = \Drupal::entityTypeManager()
          ->getStorage($definition->getBundleEntityType())
          ->loadMultiple();
        foreach ($bundles as $bundle_id => $bundle) {
          // Show a config field.
          $form['entity_config'][$definition->id()][$bundle->id()] = [
            '#type' => 'fieldset',
            '#title' => Html::escape($bundle->label()),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
          ];

          $form['entity_config'][$definition->id()][$bundle->id()]['no_field'] = [
            '#markup' => "<i>" . t('There are no File Field to configure.') . "</i>",
          ];

          $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($definition->id(), $bundle->id());
          foreach ($fields as $field_name => $field_info) {

            $options = [];
            /* If this field is a file field with pdf extension allowed,
            check to find Image field in same content type.*/
            $allowed_extensions = explode(' ', $field_info->GetSetting('file_extensions'));
            if (in_array($field_info->getType(), ['file', 'entity_reference'])) {
              if ($field_info->getType() == 'entity_reference') {
                $fld_settings = $field_info->GetSetting('handler_settings');

                if (!isset($fld_settings['target_bundles']['document'])) {
                  continue;
                }
                elseif ($fld_settings['target_bundles']['document'] != 'document') {

                  continue;
                }
              }
              elseif (!in_array("pdf", $allowed_extensions)) {
                continue;
              }
              unset($form['entity_config'][$definition->id()][$bundle->id()]['no_field']);
              $options = [
                "none" => t("Do nothing"),
                "media" => t("Media Library"),
              ];

              // Get only the images field.
              foreach ($fields as $field_img_key => $field_img_info) {
                if ($field_img_info->getType() == 'image') {

                  $image_settings = $field_img_key;
                  $image_settings .= "|" . $field_img_info->GetSetting('max_resolution');
                  $image_settings .= "|" . $field_img_info->GetSetting('min_resolution');
                  $image_settings .= "|" . $field_img_info->GetSetting('max_filesize');

                  $options[$image_settings] = Html::escape($field_img_info->getLabel() . " (" . $field_img_key . ")");
                }
              }

              $form['entity_config'][$definition->id()][$bundle->id()]["mediathumbnail_" . $definition->id() . "_" . $bundle->id() . "_" . $field_name] = [
                '#type' => 'select',
                '#title' => Html::escape($field_info->getLabel() . " (" . $field_name . ")"),
                '#options' => $options,
                '#default_value' => $config->get("mediathumbnail_" . $definition->id() . "_" . $bundle->id() . "_" . $field_name),
              ];

            }
          }
        }
      }
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxGetTerms(array &$form, FormStateInterface $form_state) {
    $form['#rebuild'] = TRUE;
    return $form['settings']['ajax-thumb-cat-wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $open_basedir = ini_get("open_basedir");

    if (!empty($open_basedir)) {
      drupal_set_message(t("open_base_dir restriction in effect. Can not check convert path."), "warning");
    }
    else {
      if (!file_exists($form_state->getValue(['mediathumbnail_convertpath']))) {
        $form_state->setErrorByName('mediathumbnail_convertpath', t('I can not find convert. Check the path and try again.'));
      }
    }

  }

}
