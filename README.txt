Media Thumbnail module provide an automatic creation
of PDF thumbnails.

With this module, you can choose which PDF filefields
must generate a thumbnail.

For each file fields you can select one of this action :
- Generate a thumbnail from a filefield
- Generate a thumbnail and store this in the media library
- Do nothing

This module requiere ImageMagick on the server (with convert).

Installation
------------
A. Install ImageMagick 7.0.1-10 on Ubuntu

Run the following commands in Terminal to install ImageMagick on Ubuntu Server:

wget http://www.imagemagick.org/download/ImageMagick.tar.gz
tar -xvf ImageMagick.tar.gz
cd ImageMagick-7.*
./configure
make
sudo make install
sudo ldconfig /usr/local/lib
sudo apt-get install ghostscript
Check that its installed properly run following command commnds in terminal
which convert
which gs

both above command should show installed paths. 
Once installed, open ImageMagick from Ubuntu Dash or Terminal.
Run "display" command (without quotes) in Terminal and press enter.

C. Module Installation

1. Copy Media Thumbnail folder to modules directory.
2. At admin/build/modules enable the Media Thumbnail module.
3. Configure the module at admin/config/media/thumbnail
